// rating.smartjs.academy/rating
//import {loadRatingData, setRatingUpdate} from './reducer';
import {loadingRidesData, loadingStationsData} from './actions/actionCreators';
import store from './store';
import MapView from './views/MapView';
import RidesListView from './views/RidesListView';
import SortControlView from './views/SortControlView';
import FiltersView from './views/FiltersView';
import RideInfoView from './views/RideInfoView';
import connect from './connect';
import connectWithFilter from './connectCombine';
import filterStore from './helpers/filterStore';


const connectRideFilter = connectWithFilter(filterStore);

const sortControlView = connect(
  store,
  new SortControlView(document.querySelector('.rides-sort-panel')),
  (state) => state.sort
);

const rideList = connectRideFilter(
  store,
  new RidesListView(document.querySelector('.rides-list')),
  (state) => state
);

const mapView = connectRideFilter(
  store,
  new MapView(document.querySelector('#map')),
  (state) => state
);

const rideInfo = connectRideFilter(
  store,
  new RideInfoView(document.querySelector('.rides-list')),
  (state) => state
);

const filtersView = connect(
  store,
  new FiltersView(document.querySelector('#ridesFilterForm')),
  (state) => state.filter
);

store.dispatch(loadingRidesData());
store.dispatch(loadingStationsData());
store.dispatch({type: "MAP_LOADING"});