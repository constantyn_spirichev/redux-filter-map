import {parsePropertiesFromString, parseFiltersPatterns} from './helpers';

const filterStore = function(){
    var currentFilters;
    var oldrides;
    var prevRes;
    return function(state){
        if(state.filter && currentFilters == state.filter && oldrides == state.rideData) {
            console.log("filter shouldn't perform, return prev results");
            console.log(state.filter);
            return {
                ...state,
                filter: state.filter,
                sort: state.sort,
                rideData: prevRes,
                };
        }
        oldrides = state.rideData;
        currentFilters = state.filter;

        if(state.filter){
            var newState = {...state}
            //console.log("filter on go: ", state.rideData.length);
            newState.rideData = newState.rideData.filter(ride => {
                let flag = [];
                for (let filter in state.filter) {
                    if(state.filter.hasOwnProperty(filter)){
                        if (typeof state.filter[filter] == "object"){
                            flag.push(state.filter[filter].reduce((res, filterValue)=>{
                                if (res === true){
                                    return true;
                                }
                                return parseFiltersPatterns(parsePropertiesFromString(ride, filter), filterValue);
                            }, false));
                        } else {
                            flag.push(parseFiltersPatterns(parsePropertiesFromString(ride, filter), state.filter[filter]));
                        }

                    }
                    
                }
                return flag.reduce((final, stepFlag)=>{
                    if(!final || !stepFlag){
                        return false;
                    }
                    return stepFlag;
                }, true);
            })
            prevRes = newState.rideData;
            return newState;
        };
        //console.log("filter done: ", state.rideData.length);
        prevRes = state.rideData;
        return state;
    };

}

export default filterStore();