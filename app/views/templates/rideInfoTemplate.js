import {template} from 'lodash';

export default template(
    "<div class='row'>" +
        "<div class='col'>" +
            "<%- from.name %> : <%- from.departureTime %> " +
            "<br >" +
            "<%- to.name %> : <%- to.arrivalTime %> " +
        "</div>" +
    "</div>"
);