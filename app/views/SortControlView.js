import BaseView from './BaseView';
import {setRidesSortin} from '../actions/actionCreators';
import store from '../store.js';

const SORT_KIT = {
  'ASC': 'DESC',
  'DESC': 'ASC',
}

export default class SortControlView extends BaseView {
  constructor(node){
    super(node);
    this._sortElements = this._node.querySelectorAll('.col[data-sort]');
    console.log(this._sortElements);
    Array.prototype.map.call(this._sortElements, (el)=>{
      el.addEventListener('click', this.sortByClick);
    });
  }
  render(sort) {
    console.log(sort, this._sortElement);
  }
  sortByClick(e){
    console.log(this.dataset);
    store.dispatch(setRidesSortin({current: this.dataset.sort, direction: this.dataset.sortdirection}));
    this.dataset.sortdirection = SORT_KIT[this.dataset.sortdirection];
    document.querySelector(".current-sort-item").classList.remove("current-sort-item");
    this.classList.add("current-sort-item");
  }
}
