export default function connectWithFilter(filter) {
  var components = [];
  var unsubscribe;
  return function(store, component, filterFn) {
    components.push(component);
    if(unsubscribe){
      unsubscribe();
    }
    unsubscribe = store.subscribe(
      () => {
        let state = store.getState();
            state = filter(state);
            components.map((comp)=>{comp.render(filterFn(state))});
      }
      );
    return unsubscribe;
  
  }
  //component.render(filterFn(store.getState()));
}
