import BaseView from './BaseView';
import rideInfoTemplate from './templates/rideInfoTemplate';

export default class RideInfoView extends BaseView {
    constructor(node){
        super(node);
        this._node.addEventListener("click", this.setSelectedRide);
    }
    render(state){
        let selectedRide = state.rideData.filter((ride)=>{
            return ride.id == state.appCondition.selectedRide
        })[0];
    
        if(state.appCondition.selectedRide && selectedRide){
            this.renderRideInfo(selectedRide);
        }
        if(state.appCondition.selectedRide === null) {
            this.removeCurrentRideInfo();
        }
        this.prevSelectedRide = selectedRide;

    }
    renderRideInfo(selectedRide){
        let rideInfoEl = document.createElement('div');
        let rideElement = document.getElementById(selectedRide.id);

        this.removeCurrentRideInfo();
        
        rideInfoEl.classList.add('ride-info');
        rideInfoEl.innerHTML = rideInfoTemplate(selectedRide);
        rideElement.appendChild(rideInfoEl);
        rideElement.classList.add('ride-info-folded');
        // scroll to selected ride
        setTimeout(()=>{
            this._node.scrollTop = 0;
            var scroll = rideInfoEl.getBoundingClientRect().top - this._node.getBoundingClientRect().top
            this._node.scrollTop = scroll;
        }, 0);
    }
    setSelectedRide(e){
        var target = e.target;
        while(target = target.parentNode) {
        if(target.classList && target.classList.contains("ride-item")){
            if(target.classList.contains("ride-info-folded")){
                store.dispatch({type: "SELECT_RIDE", rideId: null});
            } else {
                store.dispatch({type: "SELECT_RIDE", rideId: target.id});
            }
            break;
        }
        }
    }
    removeCurrentRideInfo(){
        let rideWithInfo = this._node.querySelector('.ride-info-folded');
        if(rideWithInfo) {
            rideWithInfo.removeChild(rideWithInfo.querySelector('.ride-info'));
            rideWithInfo.classList.remove('ride-info-folded');
        }
    }
}