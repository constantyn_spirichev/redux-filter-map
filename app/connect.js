import filterStore from './helpers/filterStore';

export default function connect(store, component, filterFn) {
  return store.subscribe(
    () => {
      let state = store.getState();
          component.render(filterFn(state));
    }
  );
  //component.render(filterFn(store.getState()));
}
