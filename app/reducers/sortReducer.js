import * as ACTIONS from '../actions/actionTypes';

export default function sortReducer(oldState = {}, action) {
    switch (action.type) {
        case ACTIONS.SORTING_SET:
            return action.sort;
        default:
            return oldState;
    };
}