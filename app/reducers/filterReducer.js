import * as ACTIONS from '../actions/actionTypes';

export default function filterReducer(oldState = {}, action) {
    switch (action.type) {
        case ACTIONS.FILTERING_SET:
            return action.filter;
        default:
            return oldState;
    };
}