export default class MarkerCollection {

    constructor(map) {
        this.list = [];
        this.byId = {};
        this.length = 0;
        this.map = map;
        this.layer = L.featureGroup();
        this.popUp = L.popup();
        console.log(this.layer);
    }

    add(data) {
        var marker  = L.marker(
            [data.latitude, data.longitude],
            {
                icon: this.getIcon(data.delayInSeconds),
                riseOnHover: true
            }
            ).addTo(this.layer);
            marker.id = data.id;
            marker.bindPopup(this.setPopupContent(data));
            this.list.push(marker);
            this.byId[marker.id] = marker;
            this.length = this.list.length;
            this.lastMarker = marker;
        return marker;
    }

    remove(marker) {
        let id = typeof marker === "object" ? marker.id : marker;

        this.layer.removeLayer(this.byId[id]);

        delete this.byId[id];
        this.list = this.list.filter(m => m.id != id);
        this.length = this.list.length;
        return this;
    } 

    clear() {
        this.list.map(marker =>  this.map.removeLayer(marker));
        this.list = [];
        this.byId = {};
        this.length = 0;
        return this;
    }

    update(newData) {
        this.byId[newData.id].setLatLng(
            L.point([newData.latitude, newData.longitude])
            );
        return this;
    }

    getIcon(delay){
        let className = "buss-pin ";
        if(delay >= (15 * 60) && delay < (30 * 60)) {
            className += "delay-more-15m";
        } else if(delay >= (30 * 60)) {
            className += "delay-more-30m";
        } else {
            className += "delay-less-15m";       
        }
        return L.divIcon({className});
    }

    setPopupContent(data){
        return data.lineCode + '-' + data.tripNumberCode + "<br>" + data.buses[0].numberplate;
    }

    onclick(handler){
        if(this.lastMarker){
            this.lastMarker.addEventListener('click', handler);
        }
        return this;
    }
}