export function rsDataProccessing(data) {
    let output = [];
    for (let key in data) {
        if(data.hasOwnProperty(key)){
            if(data[key].departure && data[key].arrival) {
                data[key].departureTimeString = data[key].departure.replace(/\d{4}-\d{2}-\d{2}\s(.*):\d{2}$/, '$1');
                data[key].arrivalTimeString = data[key].arrival.replace(/\d{4}-\d{2}-\d{2}\s(.*):\d{2}$/, '$1');
            }
            output.push(data[key]);
        }
    }
    return output;
}

export function parsePropertiesFromString(object, propertiesString){
    const propForArrayReg = /(\w*)\[(\d*)\]/i;

    let propList = propertiesString.split('.');

    return propList.reduce((init, prop)=>{
        let propsArray = prop.match(propForArrayReg);
        if(propsArray) {
            return init[propsArray[1]][propsArray[2]];
        } else {
            return init[prop]
        }
    }, object);
}

export function parseFiltersPatterns(value, pattern){
    let regExp = /^(>=|<=|!=|=|>|<)?([A-zäÄÖÜß\s0-9_\-=\u0430-\u044f]+)\s?(&&|\|\|)?\s?(>=|<=|!=|=|>|<|=\*)?([A-zäÄÖÜß\s0-9_\-=\u0430-\u044f]+)?/i;
    let patternMatch = pattern.match(regExp);

    if(!patternMatch){
        return true;
    }

    let relation = patternMatch[1];
    let relatedTo = patternMatch[2].replace(/\s$/, ''); // it gets unwanted space
    let logic = patternMatch[3];
    let logicRelation = patternMatch[4];
    let logicRelatedTo = patternMatch[5];

    let firstBlockResult;
    let secondBlockResult;

    let check = function(rel, val, to){
        // set non string values
        switch(to) {
            case 'false':
                to = false;
                break;
            case 'true':
                to = true;
                break;
            case 'null':
                to = null;
                break;
            case 'undefined':
                to = undefined;
                break;
        }
        
        switch(rel) {
            case ">=":
                return val >= to;
                
            case "<=":
                return val <= to;
                
            case "!=":
                return val != to
                
            case "=":
                return val == to;

            case ">":
                return val > to;
                
            case "<":
                return val < to;
            default:
                let regExp = new RegExp(to, "i");
                return val.search(regExp) >= 0;
            
        }
    };

    firstBlockResult = check(relation, value, relatedTo);
    if(!logic){
        return firstBlockResult;
    }
    secondBlockResult = check(logicRelation, value, logicRelatedTo);

    switch(logic){
        case "&&":
            return firstBlockResult && secondBlockResult;
        case "||":
            return firstBlockResult || secondBlockResult;
        default:
            throw new Error("Not valid logic operator for " + pattern);
    }
}

export function bounce(fn, delay) {
    var timeout;
    return (e) => {
        if(!timeout){
           timeout = setTimeout(()=>{ fn(e); timeout = null; }, delay);
        }
    }
}
