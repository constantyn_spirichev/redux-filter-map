import * as ACTIONS from '../actions/actionTypes';

export default function appReducer(oldState = {}, action) {
    switch (action.type) {
        case ACTIONS.RIDE_DATA_LOADING:
            return {
                ...oldState,
                rideDataLoadin: true,
            };

        case ACTIONS.RIDE_DATA_LOADED:
            return {
                ...oldState,
                rideDataLoadin: false,
                rideDataLoaded: true,
            };
        
        case ACTIONS.MAP_LOADING:
            return {
                ...oldState,
                mapLoading: true,
            };

        case ACTIONS.MAP_LOADED:
            return {
                ...oldState,
                mapLoading: false,
                mapLoaded: true,
            };
        case ACTIONS.RIDE_DATA_UPDATED:
            return {
                ...oldState,
                updatesCount: oldState.updatesCount + 1,
            };

         case ACTIONS.STATION_DATA_LOADING:
            return {
                ...oldState,
                stationDataLoadin: true,
            };

        case ACTIONS.STATION_DATA_LOADED:
            return {
                ...oldState,
                stationDataLoadin: false,
                stationDataLoaded: true,
            };

        case ACTIONS.SELECT_RIDE:
            return {
                ...oldState,
                selectedRide: action.rideId,
            }

        default:
            return oldState;
    };
}