import BaseView from './BaseView';
import store from '../store';
import {setRidesFilters} from '../actions/actionCreators';
import {bounce} from '../helpers/helpers';

export default class FiltersView extends BaseView {

    constructor(node){
        super(node);
        this._node.addEventListener('change', this.setFilterData);
        console.log(this._node.querySelectorAll('.reg-search'));
        Array.prototype.map.call(this._node.querySelectorAll('.reg-search'), (item)=>{
            item.addEventListener('keyup', bounce(this.searchKeyUp, 400));
        });
    }

    render(){

    }

    setFilterData(e){
        let formData = new FormData(this);
        let filter = {};

            formData.forEach((value, key)=>{
                if(value !== ""){
                    if(filter[key] !== undefined) {
                        
                        if(typeof filter[key] == 'object') {
                            filter[key].push(value);
                        } else {
                            filter[key] = [filter[key]];
                            filter[key].push(value);
                        }
                        
                    } else {
                        filter[key] = value;
                    }
                    
                }
            });
            store.dispatch(setRidesFilters(Object.keys(filter).length ? filter : null));
    }

    searchKeyUp(e) {
        console.log(e, e.target, this);
        //@TODO work around propper proccesss of value change
        e.target.blur();
        e.target.focus();
    }

}
