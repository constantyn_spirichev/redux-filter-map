import store from '../store';
import BaseView from './BaseView';
import rideTemplate from './templates/ridesTemplate';
import {parsePropertiesFromString} from '../helpers/helpers';

export default class RidesListView extends BaseView {

  constructor(node){
    super(node);
  }

  render(state) {
    let output = '';
    let selectedRide;
    console.log("rides in lists are the same: ", this.prevRides == state.rideData);
    console.log("sort are the same: ", this.prevSort == state.sort);

    if(this.prevSort != state.sort || this.prevRides != state.rideData){
        this.sort(state).map(ride=>{
          ride.delayTostring = this.delayToString(ride.delayInSeconds);
          output += rideTemplate(ride);
          if(state.appCondition.selectedRide == ride.id){
            selectedRide = ride;
          }
        });
        this._node.innerHTML = output;
    }
    this.prevRides = state.rideData;
    this.prevSort = state.sort;
  }

  sort(state){
    let rides = [...state.rideData];
    return rides.sort((a, b) => {
      let aValue = parsePropertiesFromString(a, state.sort.current);
      let bValue = parsePropertiesFromString(b, state.sort.current);
      
      aValue = typeof aValue == "string" ? aValue.toUpperCase() : aValue;
      bValue = typeof bValue == "string" ? bValue.toUpperCase() : bValue;

      if(aValue == undefined){
        return 1;
      }

      if(bValue == undefined){
        return -1;
      }

      if(aValue > bValue){
        return state.sort.direction == "ASC" ? 1 : -1;
      }
      if(aValue < bValue){
        return state.sort.direction == "DESC" ? 1 : -1;;
      }

      return 0;
    });
  }

  delayToString(delay) {

    if (!delay){
      return "";
    }

    let absOfDelay = Math.abs(delay);
    let hours;
    let minutes;

    hours = Math.floor(absOfDelay/3600);
    minutes = Math.ceil(60*((absOfDelay/3600)%1));

    return (delay < 0 ? "-" : "+") + hours + ":" + (minutes >= 10 ? minutes : ("0" + minutes)) + "h";

  }

  

  
}
