import * as ACTIONS from '../actions/actionTypes';

const middleware = store => next => action => {
    if(action.type == ACTIONS.MAP_LOADING){

        let script = document.createElement('script');
        let link = document.createElement('link');
        let head = document.getElementsByTagName('head')[0];
            script.src = 'http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js';
            script.onload = function(){
                store.dispatch({type: ACTIONS.MAP_LOADED});    
            };
            link.rel = 'stylesheet';
            link.href = 'http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css';
            head.appendChild(link);
            head.appendChild(script);
    }
 return next(action);
};

export default middleware;