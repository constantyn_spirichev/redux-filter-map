import * as ACTIONS from './actionTypes';

export function loadingRidesData(url = 'data/rides-data.json'){
    return {
        types: [ACTIONS.RIDE_DATA_LOADING, ACTIONS.RIDE_DATA_LOADED, ACTIONS.RIDE_DATA_UPDATED],
        request: function(){
            return fetch(url).then(r => r.json());
        },
    }
}

export function loadingStationsData(url = 'data/stations-data.json'){
    return {
        types: [ACTIONS.STATION_DATA_LOADING, ACTIONS.STATION_DATA_LOADED, ACTIONS.STATION_DATA_UPDATED],
        request: function(){
            return fetch(url).then(r => r.json());
        },
    }
}

export function setRidesSortin(sortingParam){
    return {
        type: ACTIONS.SORTING_SET,
        sort: sortingParam,
    }
}

export function setRidesFilters(filteringParam){
    return {
        type: ACTIONS.FILTERING_SET,
        filter: filteringParam,
    }
}
