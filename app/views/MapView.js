import BaseView from './BaseView';
import MarkerCollection from './classes/MarkerCollection';
import store from '../store';



class StationsMarkerCollection extends MarkerCollection {
    getIcon(){
        return L.divIcon({className: "station-pin"});
    }
    setPopupContent(data){
        return data.name;
    }
}

export default class MapView extends BaseView {
    
    render(state) {
        
        if(state.appCondition.mapLoaded && L && !this.map){
            this.map = L.map(this._node).setView([49.905249, 10.897614], 6);
            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(this.map);
            
            this.rideMarkers = new MarkerCollection(this.map);
            this.stationMarkers = new StationsMarkerCollection(this.map)

            this.setStationMarkers(state.stations);
            this.stationMarkers.layer.addTo(this.map)
            
            
            this.setRideMarkers(state.rideData);
            this.rideMarkers.layer.addTo(this.map);

            this.prevRides = state.rideData;

            this.rideMarkers.layer.setStyle({"z-index": 1000});

        } else if(state.appCondition.mapLoaded && L){
            //console.log(state);
            if(this.prevRides !==  state.rideData){
                this.updateMarkers(state.rideData);
                this.prevRides = state.rideData;
            }

            if(state.appCondition.selectedRide){
                this.focusRideMarker(state.appCondition.selectedRide);
            }
        }
    }

    setRideMarkers(rides) {
        rides.map((ride)=>{
            if(ride.latitude !== null && ride.longitude !== null) {
            this.rideMarkers.add(ride).setZIndexOffset(1000).on('click', this.selectRide);
            }
        });
        let markersGroup = new L.featureGroup(this.rideMarkers.list);

        //this.map.fitBounds(markersGroup.getBounds());
    }
    setStationMarkers(stations) {
            stations.map((station)=>{
                if(station.latitude !== null && station.longitude !== null) {
                    this.stationMarkers.add(station);
                }
            });
    }

    selectRide(){
        store.dispatch({type: "SELECT_RIDE", rideId: this.id});
    }

    updateMarkers(rides) {
        let markersSet = {...this.rideMarkers.byId};
        rides.map(ride => {
            let marker  = this.rideMarkers.byId[ride.id];

            if(marker === undefined && ride.longitude !== null && ride.latitude !== null ){
                this.rideMarkers.add(ride).on('click', this.selectRide);
            }

            if(marker){
                let location = marker.getLatLng();
                if(location.lng != ride.longitude || location.lat != ride.latitude){
                    this.rideMarkers.update(ride);
                }
                delete markersSet[marker.id]
            }

        });
        for(let marker in markersSet){
            this.rideMarkers.remove(marker);
        }
    }

    focusRideMarker(id){
        let marker = this.rideMarkers.byId[id]
        if(marker){
            marker.openPopup().bringToFront;
            this.map.panTo(marker.getLatLng());
        }
    }

    
}