import * as ACTIONS from '../actions/actionTypes';

export default function stationsReducer(oldState = [], action) {
    switch (action.type) {
        case ACTIONS.STATION_DATA_LOADED || ACTIONS.STATION_DATA_UPDATED:
            return action.data;
        default:
            return oldState;
    };
}