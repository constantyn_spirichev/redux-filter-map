import * as ACTIONS from '../actions/actionTypes';

export default function ridesReducer(oldState = [], action) {
    switch (action.type) {
        case ACTIONS.RIDE_DATA_LOADED || ACTIONS.RIDE_DATA_UPDATED:
            return action.data;
        default:
            return oldState;
    };
}