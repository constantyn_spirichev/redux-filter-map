import {template} from 'lodash';

export default template(
    "<% var departureTime = departure.replace(/\\d{4}-\\d{2}-\\d{2}\\s(.*):\\d{2}$/, '$1') %>"+
    "<% var className = ''  %>" + 
    "<% var delayInSeconds = delayInSeconds || undefined  %>" + 
    "<% if(delayInSeconds >= (15 * 60) && delayInSeconds < (30 * 60)) { %>" +
            "<% className += 'delay-more-15m' %>" +
        "<% } else if(delayInSeconds >= (30 * 60)) { %>" +
           "<% className += 'delay-more-30m' %>" +
        "<% } else if(delayInSeconds !== undefined) { %> " +
                "<% className += 'delay-less-15m' %>" + 
           "<% } else { %> " +
                "<% className += 'no-gps' %>" + 
           "<% } %>"  +
    "<div class='row ride-item' id='<%- id %>'>"+
        "<div class='col col-1 ride-status <%- className %>' > <%- delayInSeconds > (15 * 60) ? delayTostring : '' %> </div>" +
        "<div class='col col-2' title='<%- departure %>'> <%- departureTimeString %> </div>" +
        "<div class='col col-3'>" +
            "<div class='side-by-side'>" +
                "<div class='side'>" +
                    "<%- lineCode %>-<%- tripNumberCode %>" +
                "</div>" +
                "<div class='side'>" +
                    "<%- from.name %>" +
                    "<br />" +
                    "<%- to.name %>" +
                "</div>"+
            "</div>" +
        "</div>" +
        "<div class='col col-4'> <%- buses[0].partnerName %> </div>" +
        "<div class='col col-5'> <%- buses[0].numberplate %> </div>" +
    "</div>"
);