import {createStore, applyMiddleware, combineReducers} from 'redux';

import appReducer from './reducers/appReducer';
import ridesReducer from './reducers/ridesReducer';
import stationsReducer from './reducers/stationsReducer';
import filterReducer from './reducers/filterReducer';
import sortReducer from './reducers/sortReducer';

import logger from './middlewares/logger';
import async from './middlewares/async';
import map from './middlewares/map';

const initState = {
    appCondition: {
        rideDataLoadin: false,
        rideDataLoaded: false,
        stationDataLoadin: false,
        stationDataLoaded: false,
        mapLoading: false,
        mapLoaded: false,
        updatesCount: 0,
    },
    rideData: [],
    stations: [],
    filter: null,
    sort: {
        current: 'delayInSeconds',
        direction: 'ASC',
    },
};

const createStoreWithMiddleware = applyMiddleware(logger, async, map)(createStore)
const redusers = combineReducers({
    appCondition: appReducer,
    rideData: ridesReducer,
    stations: stationsReducer,
    filter: filterReducer,
    sort: sortReducer,
});

export default window.store = createStoreWithMiddleware(redusers, initState, window.devToolsExtension && window.devToolsExtension());
