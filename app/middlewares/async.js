import {rsDataProccessing} from '../helpers/helpers';

const middleware = store => next => action => {

  if (typeof action.request !== 'function') {
    return next(action);
  }

  const [loadingType, loadedType, updatedType] = action.types;

  //store.dispatch({ type: loadingType });

  return action.request().then(data => {
    store.dispatch({ type: loadedType, data: rsDataProccessing(data)});
    // setInterval(function rideUpdates(){
    //   action.request().then(data => {
    //     //let someData = ridesDataProccessing(data);
    //     store.dispatch({ type: updatedType, data: ridesDataProccessing(data)});
    //   });
    // }, 2000);
  });
}

export default middleware;
